import React, { } from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import {
    Login,
    Register
} from "../views";

export default function RootNavigation() {
    return (
        <Router>
            <Switch>
                <Route path="/" exact>
                    <Login />
                </Route>
                <Route path="/login">
                    <Login />
                </Route>
                <Route path="/register">
                    <Register />
                </Route>
                <Route path="*">
                    <div>
                        <h1>404</h1>
                        <p>Page not found</p>
                    </div>
                </Route>
            </Switch>
        </Router>
    );
}