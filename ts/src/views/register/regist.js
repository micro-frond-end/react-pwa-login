import React, { useState } from "react";
import LoadingOverlay from 'react-loading-overlay';
import { Link } from "react-router-dom";

const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);
    const [errMessage, setErrMessage] = useState('')

    // const isEmptyPass = useMemo(() => {
    //     if (password.length === 0) {
    //         return "_9ls7 hidden_elem"
    //     } else {
    //         return "_9ls7"
    //     }
    // }, [password])

    const Api = (username, password) => new Promise((resolve, reject) => {
        setTimeout(() => {
            if (username === "admin" && password === "admin") {
                resolve({
                    status: "success",
                    token: "admin"
                })
            } else {
                reject({
                    status: "error",
                    message: "Invalid credentials"
                })
            }
        }, 2500);
    })

    const handleSubmit = (evt) => {

        // can't reload page
        evt.preventDefault();
        if (email.length === 0 || password.length === 0) {
            alert("Email dan password tidak boleh kosong")
            return;
        }

        setLoading(true);
        Api(email, password).then(res => {
            console.log(res);
            window.location.href = "/";
        }).catch(err => {
            console.log(err.message)
            // alert(err.message)
            setErrMessage(err.message)
        }).finally(() => {
            setLoading(false);
        });
    }

    return (
        <div className="_li" id="u_0_3_7+">
            {
                loading && <LoadingOverlay
                    pointerEvents="none"
                    active={loading}
                    spinner
                    text='Tunggu sebentar...'
                    styles={{
                        wrapper: {
                            position: 'absolute',
                            zIndex: '10',
                            width: '100%',
                            height: '100%',
                            overflow: loading ? 'hidden' : 'scroll'
                        }
                    }}
                >
                </LoadingOverlay>
            }

            <div id="globalContainer" className="uiContextualLayerParent">
                <div className="fb_content clearfix " id="content" role="main">
                    <div>
                        <div className="_8esj _95k9 _8esf _8opv _8f3m _8ilg _8icx _8op_ _95ka">
                            <div className="_8esk">
                                <div className="_8esl">
                                    <div className="_8ice" style={{ marginTop: "112px", marginBottom: "16px" }} >
                                        <img className="fb_logo _8ilh img" src="https://static.xx.fbcdn.net/rsrc.php/y8/r/dF5SId3UHWd.svg" alt="Facebook" />
                                    </div>
                                    <h2 className="_8eso">Facebook membantu Anda terhubung dan berbagi dengan orang-orang dalam kehidupan Anda.</h2>

                                </div>
                                <form className="_8esn" onSubmit={handleSubmit}>
                                    <div className="_8iep _8icy _9ahz _9ah-">
                                        <div className="_6luv _52jv">
                                            <div className="_9vtf" data-testid="royal_login_form" id="u_0_a_Bt">
                                                <div className="_xku" id="header_block"><span className="_97w1 _50f6"><div className="_9axz">Register ke Facebook</div></span></div>
                                                <input type="hidden" name="jazoest" defaultValue={21054} autoComplete="off" />
                                                <input type="hidden" name="lsd" defaultValue="AVqSzjgu_l8" autoComplete="off" />
                                                <div className={`pam _3-95 _9ay3 uiBoxRed ${errMessage.length === 0 ? "hidden_elem" : null}`} role="alert" tabindex="0" id="error_box">
                                                    <div className="fsl fwb fcb">Kredensial Salah</div>
                                                    <div>{errMessage}</div>
                                                </div>
                                                <div>
                                                    <div className="_6lux">
                                                        <input onChange={e => setEmail(e.target.value)} type="text" className="inputtext _55r1 _6luy" name="email" id="email" placeholder="Email atau Nomor Telepon" autofocus />
                                                    </div>
                                                    <div className="_6lux">
                                                        {/* <div className="_6luy _55r1 _1kbt" id="passContainer"> */}
                                                        <input onChange={e => setPassword(e.target.value)} type="password" className="inputtext _55r1 _6luy _9npi" name="pass" id="pass" placeholder="Kata Sandi" />
                                                        {/* </div> */}
                                                    </div>
                                                </div>
                                                <input type="hidden" autoComplete="off" name="login_source" defaultValue="comet_headerless_login" />
                                                <input type="hidden" autoComplete="off" name="next" defaultValue /><div className="_6ltg">
                                                    <button
                                                        disabled={loading}
                                                        className="_42ft _4jy0 _6lth _4jy6 _4jy1 selected _51sy" name="login" data-testid="royal_login_button" type="submit" id="u_0_d_oB">
                                                        Buat Akun
                                                    </button>

                                                </div>

                                                <div className="_8icz" />
                                                <div className="_6ltg">
                                                    <Link to="/" role="button" className="_42ft _4jy0 _6lti _4jy6 _4jy2 selected _51sy" ajaxify="/reg/spotlight/" id="u_0_2_LF" data-testid="open-registration-form-button" rel="async">Masuk</Link>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="reg_pages_msg" className="_58mk">
                                            <Link to="/createPage" className="_8esh">Buat Halaman</Link> untuk selebriti, merek, atau bisnis.</div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
}

export default Login;